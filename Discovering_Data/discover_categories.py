
'''
Example webmethod for getting categories into MobilityLabs API
Please, get your credentials in https://mobilitylabs.emtmadrid.es (option REGISTER)
The authentication system returns one token for 15 minutes (self-expanding if you are using another webmethods before the expiration)
Discovering categories you can discover datacollections belongs to a specifi category)
'''



import requests
import json


#getting a valid token

url = "https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/"

headers = {
    'email': "your mail",
    'password': "your password"
    }

response = requests.request("GET", url, headers=headers)

valid_token=json.loads(response.text)["data"][0]["accessToken"]

url = "https://openapi.emtmadrid.es/v1/mobilitylabs/discover/categories/"

headers = {
    'accesstoken': valid_token

    }

response = requests.request("GET", url, headers=headers)

print(response.text)
#for this example, we can see the CD_SUBCATEGORY = 7 is the NO2 SENSORS COLLECTION