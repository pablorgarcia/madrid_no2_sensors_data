# Madrid_NO2_sensors_data

Contains an example of how to implement readers of NO2 sensors from Madrid City Council. 

Here several examples:
1. How to make a login on https://mobilitylabs.emtmadrid.es
2. How to discover collections into server storage
3. Getting data from classic API (https://openapi.emtmadrid.es) 
    Simple data.
    Using filters
4. Observing NO2 sensors using Reactive infraestructure.
    Simple data.
