
'''
Example webmethod for getting attributes for one collection (NO2) into MobilityLabs API
Please, get your credentials in https://mobilitylabs.emtmadrid.es (option REGISTER)
The authentication system returns one token for 15 minutes (self-expanding if you are using another webmethods before the expiration)
In the discover_categories.py sample you have seen how to find the CD_CATEGORY corresponding a specific dataset
This example gets the CD_SUBCATEGORY NO2 captator from Madrid City Council (id of dataset 964B4D59-A09A-4A71-8CF8-FDF219154278)
    into CATEGORY=MEDIOAMBIENTE, SUBCATEGORY":  SENSORES DE NO2 DE MADRID

'''



import requests
import json


#getting a valid token

url = "https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/"

headers = {
    'email': "your mail",
    'password': "your password"
    }

response = requests.request("GET", url, headers=headers)

valid_token=json.loads(response.text)["data"][0]["accessToken"]
#we choose collections of CD_SUBCATEGORY = 7

url = "https://openapi.emtmadrid.es/v1/mobilitylabs/discover/collections/7/"
headers = {
    'accesstoken': valid_token

    }

response = requests.request("GET", url, headers=headers)

print(response.text)
#please, observe the CD_COLLECTION code, you will need for getting data collection
#in the NO2 example, our selected category is 964B4D59-A09A-4A71-8CF8-FDF219154278