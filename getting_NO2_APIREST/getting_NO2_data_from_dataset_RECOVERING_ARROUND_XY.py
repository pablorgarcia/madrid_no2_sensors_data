
'''
Example webmethod for getting attributes for one collection (NO2) into MobilityLabs API
Please, get your credentials in https://mobilitylabs.emtmadrid.es (option REGISTER)
The authentication system returns one token for 15 minutes (self-expanding if you are using another webmethods before the expiration)
In the getting_NO2_datasets_in_collection.py sample you have seen how to find the link dataset id from a specific collection
'''



import requests
import json


#getting a valid token

url = "https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/"

headers = {
    'email': "your mail",
    'password': "your password"
    }

response = requests.request("GET", url, headers=headers)

valid_token=json.loads(response.text)["data"][0]["accessToken"]

url = "https://openapi.emtmadrid.es/v1/mobilitylabs/collection/reactive/964B4D59-A09A-4A71-8CF8-FDF219154278/1/"
#Case payload NO-EMPTY
payload = '{"geometry": {"$near": {"$geometry": {"type": "Point" ,"coordinates": [-3.693076, 40.419388 ]},"$maxDistance": 1000,"$minDistance": 0}}}'
headers = {
    'accesstoken': valid_token
    }

response = requests.request("POST", url, data=payload, headers=headers)


print(response.text)
