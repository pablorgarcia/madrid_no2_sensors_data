
'''
Example webmethod for getting attributes for one collection (NO2) into MobilityLabs API
Please, get your credentials in https://mobilitylabs.emtmadrid.es (option REGISTER)
The authentication system returns one token for 15 minutes (self-expanding if you are using another webmethods before the expiration)
In the getting_NO2_datasets_in_collection you had discover the ID category corresponding to desired datasource (NO2),
    but this datasource could be more than one dataset. Now, you may discoverd how many links to physical datasets exist into.
You may receive data in query-mode or reactive-mode from the LINK_TYPE 6
'''



import requests
import json


#getting a valid token

url = "https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/"

headers = {
    'email': "your mail",
    'password': "your password"
    }

response = requests.request("GET", url, headers=headers)

valid_token=json.loads(response.text)["data"][0]["accessToken"]
#we choose collections of CD_SUBCATEGORY = 7

url = "https://openapi.emtmadrid.es/v1/mobilitylabs/discover/collection/964B4D59-A09A-4A71-8CF8-FDF219154278/"

headers = {
    'accesstoken': valid_token

    }

response = requests.request("GET", url, headers=headers)

print(response.text)
#please, observe the CD_LINK code, you will need for getting data collection. For getting data in query format, the LINK TYPE is 6.
#we fill select SENNO2MAD.eventpos (correspondieng to "Datos de ultima lectura de sensores de NO2 del Ayuntamien to de Madrid)
#So, the CD_LINK is the 1 value.