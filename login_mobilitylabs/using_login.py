'''
Example webmethod for logging into MobilityLabs API
Please, get your credentials in https://mobilitylabs.emtmadrid.es (option REGISTER)
The authentication system returns one token for 15 minutes (self-expanding if you are using another webmethods before the expiration)
'''



import requests

url = "https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/"

headers = {
    'email': "your mail",
    'password': "your password"
    }

response = requests.request("GET", url, headers=headers)

print(response.text)